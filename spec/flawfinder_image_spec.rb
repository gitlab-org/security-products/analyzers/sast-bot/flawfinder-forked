require "tmpdir"
require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'


describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def image_name
    ENV.fetch('TMP_IMAGE', 'flawfinder:latest')
  end

  context 'with no project' do
    before(:context) do
      @output = `docker run -t --rm -w /app #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it 'shows there is no match' do
      expect(@output).to match(/no match in \/app/i)
    end

    describe 'exit code' do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context 'with test project' do

    # `successful job` is a shared example for grouping the operations
    # of running a successful scan and further validating the generated
    # report against the expected report
    shared_examples "successful job" do
      it_behaves_like "successful scan"
      describe "created report" do
        it_behaves_like "non-empty report"
        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(project) }
        end
        it_behaves_like "valid report"
      end
    end

    def parse_expected_report(expectation_name, report_name = "gl-sast-report.json")
      path = File.join(expectations_dir, expectation_name, report_name)
      if ENV['REFRESH_EXPECTED'] == "true"
        # overwrite the expected JSON with the newly generated JSON
        FileUtils.cp(scan.report_path, File.expand_path(path))
      end
      JSON.parse(File.read(path))
    end

    let(:global_vars) do
      {
        'ANALYZER_INDENT_REPORT': 'true',
        # CI_PROJECT_DIR is needed for `post-analyzers/scripts` to
        # properly resolve file locations
        # https://gitlab.com/gitlab-org/security-products/post-analyzers/scripts/-/blob/25479eae03e423cd67f2493f23d0c4f9789cdd0e/start.sh#L2
        'CI_PROJECT_DIR': '/app',
        'SEARCH_MAX_DEPTH': 20
      }
    end

    let(:project) { 'any' }
    let(:variables) { {'GITLAB_FEATURES': 'vulnerability_finding_signatures'} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables),
        report_filename: 'gl-sast-report.json')
    end

    let(:report) { scan.report }

    context 'with c' do
      let(:project) { 'c' }
      it_behaves_like 'successful job'
    end

    context 'with neverignore' do
      let(:variables) { 
        {
          'GITLAB_FEATURES': 'vulnerability_finding_signatures',
          'SAST_SCANNER_ALLOWED_CLI_OPTS': '--neverignore',
        } 
      }
      let(:project) { 'c_with_ignored_vulnerabilities' }
      it_behaves_like 'successful job'
    end
    
    context 'without neverignore' do
      let(:variables) { 
        {
          'GITLAB_FEATURES': 'vulnerability_finding_signatures',
          'SAST_SCANNER_ALLOWED_CLI_OPTS': '',
        } 
      }
      let(:project) { 'c_with_ignored_vulnerabilities' }
      it_behaves_like "recorded report" do
        let(:recorded_report) { 
          parse_expected_report(project, 'gl-sast-without-neverignore-report.json')
        }
      end
    end

    context 'with c++' do
      let(:project) { 'cplusplus' }
      it_behaves_like 'successful job'
    end

  end
end
