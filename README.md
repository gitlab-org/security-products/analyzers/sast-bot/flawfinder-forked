### [Maintenance Notice](https://docs.gitlab.com/ee/update/deprecations#sast-analyzer-consolidation-and-cicd-template-changes):
This analyzer is currently in terminal maintenance mode. No new major versions will be released. Please see our [semgrep](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep) analyzer for flawfinder rules.

# Flawfinder analyzer

This analyzer is a wrapper around [Flawfinder](https://www.dwheeler.com/flawfinder/),
a program that examines C/C++ source code and reports possible security weaknesses.
It's written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
