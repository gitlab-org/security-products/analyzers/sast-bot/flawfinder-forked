# Flawfinder analyzer changelog

## v4.3.12
- Hardcode the secure report version to `15.0.7` (!129)

## v4.3.11
- upgrade `github.com/urfave/cli/v2` version [`v2.27.1` => [`v2.27.2`](https://github.com/urfave/cli/releases/tag/v2.27.2)] (!128)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.2.0` => [`v2.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.4.0)] (!128)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.2` => [`v4.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.4.0)] (!128)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.8` => [`v2.0.9`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.9)] (!128)

## v4.3.10
- upgrade `github.com/stretchr/testify` version [`v1.8.4` => [`v1.9.0`](https://github.com/stretchr/testify/releases/tag/v1.9.0)] (!125)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.7` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!125)

## v4.3.9
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.7`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.7)] (!124)

## v4.3.8
- upgrade `github.com/urfave/cli/v2` version [`v2.25.7` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!123)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!123)

## v4.3.7
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.2.0` => [`v4.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.1)] (!120)

## v4.3.6
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.2` => [`v3.2.3`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)] (!119)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.5` => [`v4.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.2.0)] (!119)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.4` => [`v2.0.6`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.6)] (!119)

## v4.3.5
- upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.4.1`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.4.1) (!117)

## v4.3.4
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (!111)
- upgrade `github.com/stretchr/testify` version [`v1.8.0` => [`v1.8.4`](https://github.com/stretchr/testify/releases/tag/v1.8.4)] (!111)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!111)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (!111)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.2` => [`v4.1.5`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.5)] (!111)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.2` => [`v2.0.4`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.4)] (!111)

## v4.3.3
- upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.3.8`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.3.8) (!110)

## v4.3.2
- upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.3.7`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.3.7) (!109)

## v4.3.1
- Fix param parsing on `--sast-excluded-paths` and `--ignored-dirs` (!107 @nanmu42)

## v4.3.0
- Add support for flags and envs: `--sast-excluded-paths`(`SAST_EXCLUDED_PATHS`) and `--ignored-dirs`(`SEARCH_IGNORED_DIRS`) (!103 @nanmu42)

## v4.2.0
- Add [Advanced Vulnerability Tracking](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/) (!105)

## v4.1.0
- Update `ruleset` module to `v2.0.2` to support loading remote Custom Rulesets (!100)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.1.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.2)] (!100)

## v4.0.1
- Add support `SAST_SCANNER_ALLOWED_CLI_OPTS` CI variable (!93)
- Add `--neverignore` and `-n` flags under list of scanner allowed CLI options (!93)

## v4.0.0
- Upgrade report schema to v15 (!96)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.2` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!96)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!96)

## v3.2.6
- upgrade `github.com/urfave/cli/v2` version [`v2.25.0` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!97)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!97)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.18.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!97)

## v3.2.5
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.25.0`](https://github.com/urfave/cli/releases/tag/v2.25.0)] (!95)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.17.0` => [`v3.18.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.18.0)] (!95)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!95)
- update Go version to 1.19

## v3.2.4
- upgrade `github.com/urfave/cli/v2` version [`v2.23.6` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!91)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.0` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!91)

## v3.2.3
- upgrade `github.com/urfave/cli/v2` version [`v2.23.5` => [`v2.23.6`](https://github.com/urfave/cli/releases/tag/v2.23.6)] (!90)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.16.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!90)

## v3.2.2
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` => [`v2.23.5`](https://github.com/urfave/cli/releases/tag/v2.23.5)] (!86)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.0)] (!86)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.1` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!86)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.13.0` => [`v3.16.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.16.0)] (!86)

## v3.2.1
- Update common to `v3.2.1` to fix gotestsum cmd (!84)

## v3.2.0
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!81)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!81)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!81)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!81)

## v3.1.1
- Upgrade the `command` package for better analyzer messages (!80)

## v3.1.0
- Upgrade core analyzer dependencies (!79)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.0.0
- Bumping to next major version, `3.0.0` (!78)

## v2.15.0
- Update ruleset, report, and command modules to support ruleset overrides (!74)

## v2.14.8
- Update report package to v3.7.1 (!72)

## v2.14.7
- Update command package to v1.5.1 (!69)
  - Fixes issue with report output path
- Update report package to v3.6.0 (!69)
- Update ruleset package to v1.1.0 (!69)

## v2.14.6
- chore: Update go to v1.17 (!70)

## v2.14.5
- Fix range error warning (!68)

## v2.14.4
- Bump command module; add "git" executable to container (!66)

## v2.14.3
- Update to flawfinder 2.0.19 (!64)
  - Allow flawfinder to print warning messages to stderr.

## v2.14.2
- Update to flawfinder 2.0.18 (!62)

## v2.14.1
- Update to flawfinder 2.0.17 (!56)
  - Track curly brace level to reduce some problems, my thanks to Greg Myers for the work!

## v2.14.0
- Update report dependency in order to use the report schema version 14.0.0 (!53)

## v2.13.0
- Expose the flawfinder errors when it fails to run (!51)
- Output help text if a character encoding issue is encountered (!51)

## v2.12.1
- Update from flawfinder 2.0.11 to 2.0.15 (!49 @thomas-nilsson-irfu)
  - 2.0.15
    - Improved handling of LoadLibraryEx; flawfinder no longer complains
      about certain constructs that are known to be safe (eliminating
      some false positives).
  - 2.0.14
    - Various Windows improvments.
      Ignore LoadLibraryEx if its third parameter is
      LOAD_LIBRARY_SEARCH_SYSTEM32, as this is safe, and
      remove the rule for InitialCriticalSection
      (this is no longer a vulnerability on current widely-used versions
      of Windows)
    - Various C++ improvements.  Add .hpp support for C++,
      ignore "system::" to reduce false positives,
      treat ' as digit separator when file extension is a C++ file
      (for C++14).

## v2.12.0
- Update common to v2.22.0 (!47)
- Update urfave/cli to v2.3.0 (!47)

## v2.11.1
- Update logrus and cli golang dependencies (!46)

## v2.11.0
- Update common and enable disablement of custom rulesets(!44)

## v2.10.3
- Update golang dependencies (!38)

## v2.10.2
- Update golang dependencies (!35)

## v2.10.1
- Reclassify confidence level as severity (!32)

## v2.10.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!35)

## v2.9.1
- Upgrade go to version 1.15 (!33)

## v2.9.0
- Add scan object to report (!29)

## v2.8.0
- Switch to the MIT Expat license (!26)

## v2.7.1
- Update Debug output to give a better description of command that was ran (!25)

## v2.7.0
- Update logging to be standardized across analyzers (!24)

## v2.6.1
- Remove `location.dependency` from the generated SAST report (!23)

## v2.6.0
- Use alpine as base image (!18)

## v2.5.0
- Bump Flawfinder to [2.0.11](https://sourceforge.net/p/flawfinder/code/ci/2.0.11/tree/ChangeLog) (!21)

## v2.4.0
- Add `id` field to vulnerabilities in JSON report (!17)

## v2.3.0
- Add support for custom CA certs (!15)

## v2.2.1
- Use Debian Stretch as base image (!11)

## v2.2.0
- Bump Flawfinder to [2.0.10](https://sourceforge.net/p/flawfinder/code/ci/2.0.10/tree/ChangeLog)

## v2.1.0
- Bump Flawfinder to [2.0.8](https://sourceforge.net/p/flawfinder/code/ci/2.0.8/tree/ChangeLog)

## v2.0.1
- Bump common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.4.0
- Add an `Identifier` generated from the Flawfinder's function name

## v1.3.0
- Add `Scanner` property and deprecate `Tool`

## v1.2.0
- Show command error output

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
