package main

import (
	"fmt"
	"io/fs"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/flawfinder/v2/plugin"

	"github.com/bmatcuk/doublestar"
)

// listIncludedPaths resolves included path after taking out paths matched by excludedGlobs from root.
// The results are relative to root.
func listIncludedPaths(root string, excludedGlobs []string) (includedPaths []string, err error) {
	err = validateGlobPatterns(excludedGlobs)
	if err != nil {
		return
	}

	var (
		// node is hit by excludedGlobs
		excluded = make(Set)
		// ancestors of excluded nodes
		tainted = make(Set)
	)

	// first traverse finds excluded and tainted nodes
	err = filepath.WalkDir(root, func(path string, d fs.DirEntry, fileErr error) (err error) {
		if fileErr != nil {
			err = fmt.Errorf("accessing %s: %w", path, fileErr)
			return
		}
		if path == root {
			// skip the root node
			return
		}

		isDir := d.IsDir()
		name := d.Name()

		// ignored directory begins with a dot
		if isDir && strings.HasPrefix(name, ".") {
			return filepath.SkipDir
		}
		// We only care about files with C extensions
		if !isDir && !plugin.MatchFilename(name) {
			return
		}

		relPath, err := filepath.Rel(root, path)
		if fileErr != nil {
			err = fmt.Errorf("solving relative path: %w", err)
			return
		}
		if !fileMatchGlobs(relPath, excludedGlobs) {
			return
		}

		// The node is excluded and its ancestors are tainted
		excluded.Add(relPath)
		for parent := filepath.Dir(relPath); parent != "." && parent != "/"; parent = filepath.Dir(parent) {
			tainted.Add(parent)
		}

		if isDir {
			// If a directory is excluded, all of its descendant are excluded as well.
			return filepath.SkipDir
		}

		return
	})
	if err != nil {
		err = fmt.Errorf("walking root directory: %w", err)
		return
	}

	// fast path, no excluded nodes at all
	if len(excluded) == 0 {
		includedPaths = []string{"."}
		return
	}

	// second traverse finds the desired nodes
	err = filepath.WalkDir(root, func(path string, d fs.DirEntry, fileErr error) (err error) {
		if fileErr != nil {
			err = fmt.Errorf("accessing %s: %w", path, fileErr)
			return
		}
		if path == root {
			// skip the root node
			return
		}

		isDir := d.IsDir()
		name := d.Name()

		// ignored directory begins with a dot
		if isDir && strings.HasPrefix(name, ".") {
			return filepath.SkipDir
		}

		relPath, err := filepath.Rel(root, path)
		if fileErr != nil {
			err = fmt.Errorf("solving relative path: %w", err)
			return
		}
		if excluded.Has(relPath) {
			if isDir {
				// every descendant of the directory is excluded
				return filepath.SkipDir
			}

			// skip current, excluded file
			return
		}
		if tainted.Has(relPath) {
			// the directory node itself is skipped, let's dive into its children.
			return
		}

		// The node is clean

		if isDir {
			// A clean directory - all of its descendant are clean,
			// so we only need add the directory itself.
			includedPaths = append(includedPaths, relPath)
			return filepath.SkipDir
		}

		// A file, we only care file of C extensions
		if !plugin.MatchFilename(name) {
			return
		}

		includedPaths = append(includedPaths, relPath)
		return
	})
	if err != nil {
		err = fmt.Errorf("walking root directory for the second time: %w", err)
		return
	}

	return
}

// fileMatchGlobs the caller must ensure all globs are valid.
func fileMatchGlobs(path string, globs []string) (matched bool) {
	for _, glob := range globs {
		matched, _ = doublestar.PathMatch(glob, path)
		if matched {
			return
		}
	}

	return
}

func validateGlobPatterns(globs []string) (err error) {
	for _, glob := range globs {
		_, err = doublestar.PathMatch(glob, "dummy")
		if err != nil {
			err = fmt.Errorf("unexpected glob expression %q: %w", glob, err)
			return
		}
	}

	return
}

// Set is a simple implementation of string set.
type Set map[string]struct{}

// SetFromItems creates a set from provided slice items.
func SetFromItems(item ...string) Set {
	s := make(Set, len(item))
	for _, i := range item {
		s.Add(i)
	}

	return s
}

// Add adds an item into the set
func (s Set) Add(item ...string) {
	for _, i := range item {
		s[i] = struct{}{}
	}
}

// Remove removes an items from the set.
func (s Set) Remove(item string) {
	delete(s, item)
}

// Has tests if an item is in the set.
func (s Set) Has(item string) (ok bool) {
	_, ok = s[item]
	return
}

// Slice outputs the items in the set as a slice.
func (s Set) Slice() (slice []string) {
	slice = make([]string, 0, len(s))
	for item := range s {
		slice = append(slice, item)
	}

	return
}
