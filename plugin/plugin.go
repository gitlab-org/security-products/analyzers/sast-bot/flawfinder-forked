package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/plugin"
)

// MatchFilename checks the filename and makes sure it needs to be handled by Python Flawfinder.
// Ref: https://github.com/david-a-wheeler/flawfinder/blob/614801f704906a94ae420a5023c37008e22d7b95/flawfinder.py#L1995
func MatchFilename(filename string) bool {
	switch filepath.Ext(filename) {
	case ".c", ".h", ".ec", ".ecp", ".pgc", ".C", ".cpp", ".CPP", ".cxx",
		".cc", ".CC", ".c++", ".pcc", ".pc", ".sc", ".hpp", ".H":
		return true
	default:
		return false
	}
}

// Match checks the filename and makes sure it has a c/c++ extension.
func Match(path string, info os.FileInfo) (bool, error) {
	return MatchFilename(info.Name()), nil
}

func init() {
	plugin.Register("flawfinder", Match)
}
