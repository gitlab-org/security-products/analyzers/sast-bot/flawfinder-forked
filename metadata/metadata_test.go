package metadata_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/flawfinder/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "flawfinder",
		Name:    "Flawfinder",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://www.dwheeler.com/flawfinder",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
